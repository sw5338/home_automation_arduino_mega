
/*
 * This is the code for the DOOR MODULE. Some libraries come pre installed. Other libraries have to be installed from the library manager:
 * Go to SKETCH -> INCLUDE LIBRARY -> MANAGE LIBRARIES -> search 'Library Name' -> install 'Library Name'
 * 
 * The integer values assigned to variables are pin numbers on Arduino MEGA, unless stated otherwise
 * 
 * Libraries used-
 * 1.Keypad Library by Mark Stanley and Alexander Brevig
 * 2.Servo Library  Pre installed
 * 3.LiquidCrystal Pre installed
 * 4.SMTPclient.h , Mail.h -  AlertMe library
 */
 
#include<Keypad.h> // library for keypad
#include<Servo.h> //library for servo
#include<LiquidCrystal.h>//library for lcd
//#include <WiFi.h>        // Include the Wi-Fi library
//#include<ESP8266WiFi.h>
//#include <HttpClient.h>
//#include<Bridge.h>

//WiFiClient espClient;
//HttpClient httpClient;
 
//define Arduino pins to be connected to led
int dmRS = 21, dmEN = 20 , dmDB4= 4, dmDB5= 5, dmDB6 = 6, dmDB7=7;
LiquidCrystal lcdDoorModule(dmRS,dmEN,dmDB4,dmDB5,dmDB6,dmDB7); // lcd object

//define a servo object
Servo doorServo;

//using 4x4 keypad
const byte ROWS = 4; // no of rows 
const byte COLS = 4;  // no of cols
char hexaKeys[ROWS][COLS] = {
  {'1', '2', '3', 'A'},
  {'4', '5', '6', 'B'},
  {'7', '8', '9', 'C'},
  {'*', '0', '#', 'D'}
}; //keypad structure- written in this way for better visualization

byte rowArduinoPins[ROWS] = {44, 46, 48, 50}; //row pins connected to arduino 
byte colArduinoPins[COLS] = {45, 47, 49, 51}; //col pins connected to arduino

//create keypad and its object
Keypad doorKeypad = Keypad(makeKeymap(hexaKeys), rowArduinoPins, colArduinoPins, ROWS, COLS); 

//define Class doorModule for storing funcs and vars of this module
class doorModule{

private:

  //everything related to internet communication
/*  struct networkSettings{

      char ssid[]; // wifi name
      char wifiPassword[]; // wifi password
      char emailID[]; // enter the email address in 'Base64' encoding
      char emailPassword[]; // enter the password in 'Base64' encoding
      char server[];
          
  }ns;*/
  
  //everything related to Servo motor
  struct servoSettings{
    
      Servo doorServo;
      int servoPin;
  }ss;  //object of structure

  //everything related for logging into home
  struct loginSettings{
    
    char passkey[16]; //set Password - max 16 characters 
    char resetValue[16]; // set Reset value, which resets password to a default value;
  //  char defaultPasskey[16]; // default Password on initializing reset sequence
    char input[16]; // input of user
    int triesLeft; // no of tries available
    int ctr; //counter for position of input
    int printLen; //value of string length for wrong password input

  }ls;
    
    int irPin;
    int buzzerPin; //buzzer and red led controlled thru one pin
    int greenLed; // pin for green led
    int redLed; //pin for red led
    float Time,prevTime; // time and prevtime, for time gap to understand user has entered his code
    bool authValue; // value acquired after authentication
    
    bool authenticate(); //function to authenticate passkey
    void servoControl(); //control servo opening and closing
    void resetPassword();
    void clearHalfScreen(int &ctr); // clear half part of screen
    void welcomeMessage(); // welcome message of lcd
   // bool wifiConnect();
   // void sendMessage(); 
          
public:

 // void httpGetRequest();
  void initializeValues(); // initialize all variable values
  void setupObjects(); //define objects for all modules
  void login(); // login to home
  
};

doorModule dm; //class object

void setup() {

    dm.initializeValues(); //function called, once
}

void loop() {
    dm.login(); // function called, in loop
}



/* 
 *  Declared Functions of Door Module
 */


//initialize values of all variables
void doorModule::initializeValues(){

  //default values for the following vars:
  ls.ctr=0;
  ls.printLen=14;
  
  //Arduino pin connected to servo
  ss.servoPin = 2;
  
//buzzer + led pin
  buzzerPin=28;
  greenLed=29;
  redLed = 30;
  irPin = 31;

  authValue = false;
  strcpy(ls.resetValue,"ABCD1234567890"); // -----> Set a secret reset sequence which resets password to default Value
  //strcpy(ls.defaultPasskey,"111222"); // set Default password when reset sequence is run
  strcpy(ls.passkey,"123456"); // ----> Set your Password here  or use 'defaultPasskey'

/*  strcpy(ns.ssid,"Africa");
  strcpy(ns.wifiPassword,"John1234");
  strcpy(ns.emailID,"am9hb3BlZHJvMjA4MEBob3RtYWlsLmNvbQo=");
  strcpy(ns.emailPassword,"Sm9hb3BlZHJvMSsK");
  strcpy(ns.server,"mail.smtp2go.com");
  */
  ls.triesLeft=3;

  setupObjects();
}
//--------------------------------------------------------------------------------------------------------------------------------------------------/

//declare objects of all modules
void doorModule::setupObjects(){

  //begin serial connection
  Serial.begin(9600);

  
  //begin wifi connection
  /*authValue = wifiConnect();

  if(authValue)
  {
    Serial.println("Connected!");
  }

  else
  {
    Serial.println("Connection Timeout...");
  }

  //begin bridge connection for http requests
  Bridge.begin();
  */
  //connect pin to servo
  ss.doorServo.attach(ss.servoPin);
  ss.doorServo.write(0);

//set LEDs
  pinMode(buzzerPin,OUTPUT);
  pinMode(greenLed,OUTPUT);
  pinMode(redLed,OUTPUT);
  pinMode(irPin,INPUT);

  digitalWrite(buzzerPin,LOW);
  digitalWrite(greenLed,LOW);
  digitalWrite(redLed,LOW);
  
  //lcd object
  lcdDoorModule.begin(16,2);
  welcomeMessage();

  //print password
  Serial.println(ls.passkey);

  authValue= false;
  //start time 
  prevTime = millis();
  Time = prevTime;
}

//--------------------------------------------------------------------------------------------------------------------------------------------------/

//control servoMotor to rotate door
void doorModule::servoControl(){

  ss.doorServo.write(90);//rotate by 90º

  //wait if someone is at the door
  while(digitalRead(irPin)==false);
  
  ss.doorServo.write(0); //rotate to origin
}

//--------------------------------------------------------------------------------------------------------------------------------------------------/

//reset the password
void doorModule::resetPassword()
{
  lcdDoorModule.clear();
  lcdDoorModule.setCursor(0,0);
  lcdDoorModule.print("Input new password:");
  strcpy(ls.passkey,"");

  ls.passkey[ls.ctr]= doorKeypad.getKey(); //get key input

  if(!(ls.passkey[ls.ctr] == NO_KEY) && ls.passkey[ls.ctr]!='*') //if input found and not *
  {
    //backspace
    if(ls.passkey[ls.ctr]=='#' && ls.ctr!=0)
    {
      ls.passkey[ls.ctr]="\0"; // make current value null
      ls.ctr--;//decrement ctr val
      ls.input[ls.ctr]="\0";//make previous value null
      lcdDoorModule.setCursor(ls.ctr,1);
      lcdDoorModule.print(" ");
      lcdDoorModule.setCursor(ls.ctr,1);
    }
    
    else if(ls.passkey[ls.ctr]=='#' && ls.ctr==0);
//if digit
    else
    { 
      Serial.println(ls.input[ls.ctr]);
      ls.ctr++;
      lcdDoorModule.print("*"); //print * on lcd
    }
    delay(50);
  
  }

  else if(ls.passkey[ls.ctr]=='*')
  {
    ls.passkey[ls.ctr]='\0';
    Serial.println(ls.passkey);
  }

  ls.ctr=0;
}

///------------------------------------------------------------------------------------------------------------------------------------------------/

//function to authenticate input and passkey
bool doorModule::authenticate()
{
  //reset sequence
  if (strcmp(ls.input,ls.resetValue)==0)
  {
   // strcpy(ls.passkey,ls.defaultPasskey);
    resetPassword();
    return true;
  }

  //passkey confirmed
  else if(strcmp(ls.input,ls.passkey)==0)
    return true;

  //wrong input
  else
    return false;
}

///------------------------------------------------------------------------------------------------------------------------------------------------/

//clear half part of LCD
void doorModule::clearHalfScreen(int &ctr){

    for(ctr;ctr>=0;ctr--)
     {
        lcdDoorModule.setCursor(ctr,1);
        lcdDoorModule.print(" ");
     }
     ctr=0;
}

///------------------------------------------------------------------------------------------------------------------------------------------------/

//welcome Message on lcd
void doorModule::welcomeMessage(){

  lcdDoorModule.clear();
  lcdDoorModule.setCursor(0,0);
  lcdDoorModule.print("WELCOME! Pin:"); // max 16 letterso
  lcdDoorModule.setCursor(0,1);
}

///------------------------------------------------------------------------------------------------------------------------------------------------/
/*
//connect to wifi
bool doorModule::wifiConnect(){

  int i=0;
  
    WiFi.begin(ns.ssid, ns.wifiPassword);

    while(WiFi.status() != WL_CONNECTED)
    {
      if(i==60)
      {
        Serial.println(WiFi.status())
        return false;
      }
      i++;
      delay(100);
      
    }

    return true;
}

///------------------------------------------------------------------------------------------------------------------------------------------------/

//email notification
void doorModule::sendMessage()
{
  espClient.connect(ns.server, 2525); //connect to server,port

  espClient.println(ns.emailID); //send email
  espClient.println(ns.emailPassword); // send password

  espClient.println(F("MAIL From: joaopedro2080@hotmail.com"));
  espClient.println(F("RCPT To: joaopedro2080@hotmail.com"));
  
  espClient.println(F("DATA"));

  // change to recipient address
  espClient.println(F("To: joaopedro2080@hotmail.com"));
  // change to your address
  espClient.println(F("From: joaopedro2080@hotmail.com"));
  espClient.println(F("Subject: Intruder Alert!!!\r\n"));
  espClient.println(F("The pin has been entered incorrectly 3 times.\n"));
  espClient.println(F("There might be an intruder! Your house is in danger!"));

  espClient.stop();
}

void doorModule::httpGetRequest()
{
  char command[]="";
  
  httpClient.get("http://ptsv2.com/t/z0or9-1554905659/post");

  while(httpClient.available())
  {
    strcat(command,httpClient.read());
  }

  Serial.print(command);

  if(strcmp(command,"turn off alarm")==0)
  {
    digitalWrite(buzzerPin,LOW);
    digitalWrite(redLed,LOW);

    Serial.print("ok");
  }
}
*/

///------------------------------------------------------------------------------------------------------------------------------------------------/

//login and enter house
void doorModule::login(){
//ik   
  ls.input[ls.ctr]= doorKeypad.getKey(); //get key input

  if(!(ls.input[ls.ctr] == NO_KEY) && ls.input[ls.ctr]!='*') //if input found and not *
  {
    //Time = millis(); //start timer

    //backspace
    if(ls.input[ls.ctr]=='#' && ls.ctr!=0)
    {
      ls.input[ls.ctr]="\0"; // make current value null
      ls.ctr--;//decrement ctr val
      ls.input[ls.ctr]="\0";//make previous value null
      lcdDoorModule.setCursor(ls.ctr,1);
      lcdDoorModule.print(" ");
      lcdDoorModule.setCursor(ls.ctr,1);
    }
    
    else if(ls.input[ls.ctr]=='#' && ls.ctr==0);

//if digit
    else
    { 
      Serial.println(ls.input[ls.ctr]);
      ls.ctr++;
      lcdDoorModule.print("*"); //print * on lcd
    }
    delay(50);
  }

  else if (ls.input[ls.ctr]=='*') // if input is *
  {
    ls.input[ls.ctr]='\0';
    Serial.println(ls.input);
      
      //Time = prevTime;
      authValue = authenticate(); //call function authenticate, and store value to authValue
  
  if(authValue==true) // if auth value = true
  {
    //show green signal
    digitalWrite(buzzerPin,LOW);
    digitalWrite(redLed,LOW);
    digitalWrite(greenLed,HIGH);

    //print on lcd
    lcdDoorModule.clear();
    lcdDoorModule.setCursor(0,0);
    lcdDoorModule.print("Pin CORRECT!");
    lcdDoorModule.setCursor(0,1);
    lcdDoorModule.print("WELCOME HOME");

    Serial.println("Welcome HOME!");
    //open door
    servoControl();

     //reset each value
     ls.ctr = 0;
     strcpy(ls.input,"");
     Serial.println(ls.input);
     ls.triesLeft=3;   

     //wait for key
      while(doorKeypad.getKey() == NO_KEY);
      
     //turn off signal
     welcomeMessage();
     digitalWrite(greenLed,LOW);
      
  }

  else //if auth value = false
  {
    if(ls.triesLeft == 0) // if no tries left
    {  
        lcdDoorModule.clear();
        lcdDoorModule.setCursor(0,1);
        lcdDoorModule.print("Too many fails!"); // print on lcd

        digitalWrite(buzzerPin,HIGH); //buzzer buzzes
        digitalWrite(redLed,HIGH); //red led glows

   //     sendMessage();
        
        delay(5000);
       // ls.triesLeft =3;
    }
  
    else
    {
        ls.triesLeft--; //remove a try

        //print on serial monitor
        Serial.print("WRONG PASSWORD!");
        
        //clear half part of LCD
        clearHalfScreen(ls.ctr);
        
        //print on lcd
        lcdDoorModule.setCursor(0,1);
        lcdDoorModule.print("WRONG PASSWORD!");

        digitalWrite(redLed,HIGH); // red led glows
        
        //5 second delay
        delay(5000);
        
        //show welcome message
        welcomeMessage();
        
        //reset values
        ls.ctr = 0;
        ls.printLen = 14;
        strcpy(ls.input,"");      
    }
  }
 }
}

///------------------------------------------------------------------------------------------------------------------------------------------------/
