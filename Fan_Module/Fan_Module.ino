
/*
 * This is the code for the FAN MODULE. Some libraries come pre installed. Other libraries have to be installed from the library manager:
 * Go to SKETCH -> INCLUDE LIBRARY -> MANAGE LIBRARIES -> search 'Library Name' -> install 'Library Name'
 * 
 * The integer values assigned to variables are pin numbers on Arduino MEGA, unless stated otherwise
 */
#include<LiquidCrystal.h>
#include<SPI.h>
#include<TimerOne.h>
#include<max6675.h>

int fmDB4=22, fmDB5=23, fmDB6=24, fmDB7=25, fmRS=26, fmEN=27;// Arduino pins connected to the LCD Terminals
LiquidCrystal lcdFanModule(fmRS, fmEN, fmDB4, fmDB5, fmDB6, fmDB7);

int max_CS=11, max_SO=12, max_SCK=13;
MAX6675 thermocouple(max_SCK, max_CS, max_SO);

class fanModule{
    private:
    
     struct pidSettings{
      
        float pidError;
        float prevError;
        int pidValue;
        int kp, ki, kd ;
        int pidP, pidI, pidD;
     }ps;

     /*struct maxSettings{
        int max_CS, max_SO , max_SCK; 
     }ms;*/

     struct encoderSettings{ 
      int encoderDT, encoderCLK, encoderButton;
      //Variables for rotary encoder state detection
      int clkState;
      int lastState;       
     }es;
     
      int motorPin;
      float temperature, ambientTemperature;
      float defaultAmbientTemperature;
      float elapsedTime, Time, timePrev;
 
public:
      void initializeValues();
      void setupObjects();
      //double readThermocouple();
      void setTemperature();
      void fanControl();
      void displayTemperature();
          
};

fanModule fm;
void setup() {
 
  fm.initializeValues();
 
}

void loop() {

  fm.setTemperature();
  fm.fanControl();
  fm.displayTemperature();
  
}




/* Functions of Fan_Module
 * Order as declared in class 
 */


//Initialize all variable values
void fanModule::initializeValues()
{
  
   defaultAmbientTemperature = 24.5; // default threshold temperature
   ambientTemperature = defaultAmbientTemperature;
  
   //connect motor to arduino pin
   motorPin=3;

   //MAX6675 pins
   max_CS = 11;
   max_SO = 12;
   max_SCK = 13; 

   //set default pid values
   ps.pidError = 0;
   ps.prevError = 0; 
   ps.pidValue=0;
   
  //set constants for pid
   ps.kp = 9.1;    ps.ki = 0.3;    ps.kd = 1.8;
   ps.pidP = 0;   ps.pidI = 0;     ps.pidD = 0;
  
  //set encoder pins
  es.encoderDT= 8;
  es.encoderCLK = 9;
  es.encoderButton = 10;
  
  setupObjects();
}


//define objects and pins
void fanModule::setupObjects(){

 //setup MAX6675
  pinMode(max_CS, OUTPUT);
  pinMode(max_SO, INPUT);
  pinMode(max_SCK, OUTPUT);

  //setup motor
  pinMode(motorPin,OUTPUT);
  
  //setup encoder
  pinMode(es.encoderDT,INPUT_PULLUP);
  pinMode(es.encoderCLK,INPUT_PULLUP);
  pinMode(es.encoderButton, INPUT_PULLUP);

  //read status of encoder
  es.lastState = digitalRead(es.encoderCLK);
  Time = millis();
  
}

//Read Rotatory encoder
void fanModule::setTemperature(){
  es.clkState = digitalRead(es.encoderCLK);
  

  if (es.clkState != es.lastState){
    
    if(digitalRead(es.encoderDT) != es.clkState){
        ambientTemperature += 0.5;
    }
    else{
        ambientTemperature -=0.5;
    }
  }

  es.lastState = es.clkState;
  defaultAmbientTemperature = ambientTemperature;
  
}

//control fan speed according to temperature
void fanModule::fanControl (){
  
  temperature = thermocouple.readCelsius(); // use readFahrenheit() for ºF
  
  ps.pidError = ambientTemperature - temperature + 3;
  //Calculate the P value
  ps.pidP = 0.01*ps.kp * ps.pidError;
  //Calculate the I value in a range on +-3
  ps.pidI = 0.01*ps.pidI + (ps.ki * ps.pidError);
  

  //For derivative we need real time to calculate speed change rate
  timePrev = Time;                            // the previous time is stored before the actual time read
  Time = millis();     // actual time read
  elapsedTime = (Time - timePrev) / 1000; 
  //Now we can calculate the D calue
  ps.pidD = 0.01*ps.kd*((ps.pidError - ps.prevError)/elapsedTime);
  //Final total PID value is the sum of P + I + D
  ps.pidValue = ps.pidP + ps.pidI + ps.pidD;
  
  if(ps.pidValue < 0)
  {    ps.pidValue = 0;    }
  if(ps.pidValue > 255)  
  {    ps.pidValue = 255;  }

  //The MOSFET is activated with a 0 to the base of the BJT, 255-PID value (inverted)
  analogWrite(motorPin,255-ps.pidValue);
  
  ps.prevError = ps.pidError;  

  delay(250);

}

//display temperature
void fanModule::displayTemperature()
{
  lcdFanModule.setCursor(0,0);

  lcdFanModule.print("Temperature: ");
  lcdFanModule.print(temperature);
}

/*double fanModule::readThermocouple() {

  uint16_t value;
   
  digitalWrite(max_CS, LOW);
  delay(1);

  // Read in 16 bits
  
  value = shiftIn(max_SO, max_SCK, MSBFIRST);
  value <<= 8; //extract operator
  v |= shiftIn(max_SO, max_SCK, MSBFIRST);
  
  digitalWrite(max_CS, HIGH);
  
  if (v & 0x4) 
  {    
    // 0x4 is Bit 2, indicates if the thermocouple is disconnected
    return false;     
  }

  // The lower three bits (0,1,2) are discarded status bits
  v >>= 3;

  // The remaining bits are the number of 0.25 degree (C) counts
  return v*0.25;
}
*/
